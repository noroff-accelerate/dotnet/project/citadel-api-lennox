﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class User
    {
        [Key]
        [Required]
        public string UserId { get; set; }

        public string UserName { get; set; }

        // Reference for keycloak
        public string UserToken { get; set; }
#nullable enable
        [DataType(DataType.ImageUrl)]
        public string? ProfileImg { get; set; }
#nullable disable
        // Navigation properties
        public ICollection<Loan> Loans { get; set; }
        public ICollection<UserFavourite> UserFavourites { get; set; }
    }
}
