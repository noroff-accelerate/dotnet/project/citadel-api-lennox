﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class Book
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookId { get; set; }

        [MaxLength(13)]
        public string ISBN10 { get; set; }

        [MaxLength(17)]
        public string ISBN13 { get; set; }

        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(50)]
        public string Genre { get; set; }

        public string Edition { get; set; }

        public string Keywords { get; set; }

#nullable enable
        [MaxLength(50)]
        public string? Publisher { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ReleaseDate { get; set; }

        [MaxLength(50)]
        public string? Format { get; set; }

        [MaxLength(50)]
        public string? Language { get; set; }

        public int? Length { get; set; }

        public string? Description { get; set; }

        [DataType(DataType.ImageUrl)]
        public string? CoverImg { get; set; }
#nullable disable

        // Possible additions for loans logic
        public int QtyOnHand { get; set; }

        /* 
        This is a navigation property linking tables 
        */
        public ICollection<BookAuthor> BookAuthors { get; set; }
        public ICollection<BookLoan> BookLoans { get; set; }
        public ICollection<UserFavourite> UserFavourites { get; set; }
    }
}
