﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class BookAuthor
    {
        [Key]
        [Required]
        public int BookId { get; set; }
        // Navigation property to Book class
        public Book Book { get; set; }

        [Key]
        [Required]
        public int AuthorId { get; set; }
        // Navigation property to Author class
        public Author Author { get; set; }
    }
}
