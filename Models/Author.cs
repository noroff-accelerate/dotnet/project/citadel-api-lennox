﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class Author
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }

        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        /* 
        This is a navigation property to BookAuthor, 
        the linking table between Book and Author.
        */
        public ICollection<BookAuthor> BookAuthors { get; set; }
    }
}
