﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class Loan
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LoanId { get; set; }

        // Navigation property to BookLoans class
        public ICollection<BookLoan> BookLoans { get; set; }

        [Required]
        public string UserId { get; set; }
        // Navigation property to Profile class
        public User User { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        [MaxLength(10)]
        public string Status { get; set; } = "Loaned";
    }
}
