﻿using AutoMapper;
using bookstore_api.DTOs.Loan;
using bookstore_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Profiles
{
    public class LoanProfile : Profile
    {
        public LoanProfile()
        {
            CreateMap<LoanDto, Loan>();
            CreateMap<LoanCreateDto, Loan>().ReverseMap();
            CreateMap<Loan, LoanDto>().ForMember(l=>l.Books, opt=>opt.MapFrom(b=>b.BookLoans));
        }
    }
}
