﻿using AutoMapper;
using bookstore_api.DTOs.Book;
using bookstore_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<BookCreateDto, Book>().ReverseMap();
            CreateMap<BookReadShortDto, Book>().ReverseMap();
            CreateMap<Book, BookDto>().ForMember(bdto => bdto.Authors, 
                opt=> opt.MapFrom(b => b.BookAuthors));
            CreateMap<Book, BookReadGalleryDto>().ForMember(bg => bg.Authors,
                opt => opt.MapFrom(b => b.BookAuthors));
            // For use in loans - linking title form a book in a loan to the bookreadshortdto used in the loandto
            CreateMap<BookLoan, BookReadShortDto>().ForMember(br => br.Title, opt => opt.MapFrom(bl => bl.Book.Title)); ;
        }
    }
}
