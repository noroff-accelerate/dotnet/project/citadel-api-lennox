﻿using AutoMapper;
using bookstore_api.DTOs.Author;
using bookstore_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            CreateMap<AuthorDto, Author>().ReverseMap();
            CreateMap<AuthorCreateDto, Author>().ReverseMap();
            // Where AuthorDto is used in books
            CreateMap<BookAuthor, AuthorDto>().ForMember(adto=>adto.Name, opt => opt.MapFrom(ba=>ba.Author.Name));
        }
    }
}
