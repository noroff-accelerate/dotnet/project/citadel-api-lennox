﻿using AutoMapper;
using bookstore_api.DTOs.User;
using bookstore_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadShortDto>().ReverseMap();
        }

    }
}
