﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
using bookstore_api.DTOs.Book;
using bookstore_api.DTOs.Author;
using AutoMapper;

namespace bookstore_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookDbContext _context;
        private readonly IMapper _mapper;

        public BooksController(BookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CommonResponse<BookReadGalleryDto>>>> GetBook()
        {
            // Make CommonResponse object to use
            CommonResponse<IEnumerable<BookReadGalleryDto>> resp = new CommonResponse<IEnumerable<BookReadGalleryDto>>();
            var model_books = await _context.Book.Include(b=>b.BookAuthors).ThenInclude(ba => ba.Author).ToListAsync();

            // Want to return a list of books in a dto, use mapper to map from model to dto
            List<BookReadGalleryDto> books = _mapper.Map<List<BookReadGalleryDto>>(model_books);

            resp.Data = books;
            return Ok(resp);
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CommonResponse<BookDto>>> GetBook(int id)
        {
            // Make CommonResponse object to use
            CommonResponse<BookDto> resp = new CommonResponse<BookDto>();
            // Fetch the book with all authors
            var book_model = await _context.Book.Include(b => b.BookAuthors).ThenInclude(ba=>ba.Author).FirstOrDefaultAsync(b => b.BookId == id);

            if (book_model == null)
            {
                resp.Error = new Error { Status = 404, Message = "Cannot find an book with that Id" };
                return NotFound(resp);
            }

            // Map to dto and return
            resp.Data = _mapper.Map<BookDto>(book_model);

            return Ok(resp);
        }

        // PUT: api/Books/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, BookDto book)
        {
            // Create response object
            CommonResponse<BookDto> resp = new CommonResponse<BookDto>();

            if (id != book.BookId)
            {
                resp.Error = new Error { Status = 400, Message = "There was a mismatch with the provided id and the object." };
                return BadRequest(resp);
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Books
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CommonResponse<BookDto>>> PostBook(BookCreateDto book)
        {
            // Create response object
            CommonResponse<BookDto> resp = new CommonResponse<BookDto>();

            if (!ModelState.IsValid)
            {
                resp.Error = new Error
                {
                    Status = 400,
                    Message = "The book did not pass validation, ensure it is in the correct format."
                };
                return BadRequest(resp);
            }
            // Map to model class to add to db
            Book book_model = _mapper.Map<Book>(book);

            // Try to add - if it fails here there is something wrong with the server
            try
            {
                _context.Book.Add(book_model);
                await _context.SaveChangesAsync();
                // Add authors
                foreach (int a_id in book.Authors)
                {
                    BookAuthor ba = new BookAuthor() { BookId = book_model.BookId, AuthorId = a_id };
                    _context.BookAuthor.Add(ba);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                resp.Error = new Error { Status = 500, Message = e.Message };
                return StatusCode(StatusCodes.Status500InternalServerError, resp);
            }

            var a = book_model.BookAuthors.First().Author;

            var authors = _mapper.Map<List<AuthorDto>>(book_model.BookAuthors);

            // Need to make response object, mapping back to dto
            resp.Data = _mapper.Map<BookDto>(book_model);
            // Gets here if its a success
            return CreatedAtAction("GetBook", new { id = resp.Data.BookId }, resp);
        }

        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CommonResponse<BookDto>>> DeleteBook(int id)
        {
            // Make response object
            CommonResponse<BookDto> resp = new CommonResponse<BookDto>();
            var book = await _context.Book.FindAsync(id);
            if (book == null)
            {
                resp.Error = new Error { Status = 404, Message = "A book with that id could not be found." };
                return NotFound(resp);
            }

            _context.Book.Remove(book);
            await _context.SaveChangesAsync();

            resp.Data = _mapper.Map<BookDto>(book);

            return Ok(resp);
        }

        private bool BookExists(int id)
        {
            return _context.Book.Any(e => e.BookId == id);
        }
    }
}
