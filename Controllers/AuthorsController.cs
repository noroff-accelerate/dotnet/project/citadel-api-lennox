﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
using bookstore_api.DTOs.Author;
using AutoMapper;

namespace bookstore_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly BookDbContext _context;
        private readonly IMapper _mapper;

        public AuthorsController(BookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Authors
        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<AuthorDto>>>> GetAuthor()
        {
            // Make CommonResponse object to use - IEnumerable means its a list of the object
            CommonResponse<IEnumerable<AuthorDto>> resp = new CommonResponse<IEnumerable<AuthorDto>>();
            // Fetch list of model class and map to dto
            var model_authors = await _context.Author.ToListAsync();
            List<AuthorDto> au = _mapper.Map<List<AuthorDto>>(model_authors);
            // Return the data
            resp.Data = au;
            return Ok(resp);
        }

        // GET: api/Authors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CommonResponse<AuthorDto>>> GetAuthor(int id)
        {
            // Make CommonResponse object to use
            CommonResponse<AuthorDto> resp = new CommonResponse<AuthorDto>();

            var author_model = await _context.Author.FindAsync(id);

            if (author_model == null)
            {
                resp.Error = new Error { Status = 404, Message = "Cannot find an author with that Id" };
                return NotFound(resp);
            }

            // Map from Author model class to AuthorDto
            resp.Data = _mapper.Map<AuthorDto>(author_model);

            return Ok(resp);
        }

        // PUT: api/Authors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, AuthorDto author)
        {
            // Create response object
            CommonResponse<AuthorDto> resp = new CommonResponse<AuthorDto>();
            if (id != author.AuthorId)
            {
                resp.Error = new Error { Status = 400, Message = "There was a mismatch with the provided id and the object." };
                return BadRequest(resp);
            }

            _context.Entry(author).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Authors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CommonResponse<AuthorDto>>> PostAuthor(AuthorCreateDto author)
        {
            // Make response object
            CommonResponse<AuthorDto> resp = new CommonResponse<AuthorDto>();

            // See if the author is valid
            if (!ModelState.IsValid)
            {
                resp.Error = new Error { Status = 400, 
                    Message = "The author did not pass validation, ensure it is in the correct format." };
                return BadRequest(resp);
            }

            // Map from AuthorCreateDto to Author model class
            Author author_model = _mapper.Map<Author>(author);

            // Try to add - if it fails here there is something wrong with the server
            try
            {
                _context.Author.Add(author_model);
                await _context.SaveChangesAsync();
            } catch (Exception e)
            {
                resp.Error = new Error { Status = 500, Message = e.Message };
                return StatusCode(StatusCodes.Status500InternalServerError, resp);
            }


            // Map to AuthorDto to return without navigation properties
            // Gets here if its a success
            resp.Data = _mapper.Map<AuthorDto>(author_model);
            return CreatedAtAction("GetAuthor", new { id = resp.Data.AuthorId }, resp);
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CommonResponse<AuthorDto>>> DeleteAuthor(int id)
        {
            // Make response object
            CommonResponse<AuthorDto> resp = new CommonResponse<AuthorDto>();

            var author = await _context.Author.FindAsync(id);
            if (author == null)
            {
                resp.Error = new Error { Status = 404, Message = "An author with that id could not be found." };
                return NotFound(resp);
            }

            _context.Author.Remove(author);
            await _context.SaveChangesAsync();

            // Map model class to dto to drop navigation properties
            resp.Data = _mapper.Map<AuthorDto>(author);

            return Ok(resp);
        }

        private bool AuthorExists(int id)
        {
            return _context.Author.Any(e => e.AuthorId == id);
        }
    }
}
