﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
using bookstore_api.DTOs.Loan;
using AutoMapper;

namespace bookstore_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoansController : ControllerBase
    {
        private readonly BookDbContext _context;
        private readonly IMapper _mapper;

        public LoansController(BookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Loans
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CommonResponse<LoanDto>>>> GetLoan()
        {
            // Make CommonResponse object to use - IEnumerable means its a list of the object
            CommonResponse<IEnumerable<LoanDto>> resp = new CommonResponse<IEnumerable<LoanDto>>();
            // Fetch list of model class and map to dto
            var loan_models = await _context.Loan.Include(l=>l.User).Include(l=>l.BookLoans).ThenInclude(bl=>bl.Book).ToListAsync();
            List<LoanDto> books = _mapper.Map<List<LoanDto>>(loan_models);

            // Return data
            resp.Data = books;

            return Ok(resp);
        }

        // GET: api/Loans/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Loan>> GetLoan(int id)
        {
            var loan = await _context.Loan.FindAsync(id);

            if (loan == null)
            {
                return NotFound();
            }

            return loan;
        }

        // PUT: api/Loans/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoan(int id, Loan loan)
        {
            if (id != loan.LoanId)
            {
                return BadRequest();
            }

            _context.Entry(loan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Loans
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CommonResponse<LoanDto>>> PostMaLoan(LoanCreateDto loan)
        {
            // Make CommonResponse object to use
            CommonResponse<LoanDto> resp = new CommonResponse<LoanDto>();

            // Map to model class
            var loan_model = _mapper.Map<Loan>(loan);

            // Add to db
            _context.Loan.Add(loan_model);
            await _context.SaveChangesAsync();
            foreach(var b in loan.LoanedBooks)
            {
                _context.BookLoan.Add(new BookLoan { BookId = b, LoanId = loan_model.LoanId });
                await _context.SaveChangesAsync();
            }

            // Return newly created resource and locaiton
            var l = await _context.Loan.Include(l => l.User).Include(l => l.BookLoans).ThenInclude(bl=>bl.Book).FirstOrDefaultAsync(l=>l.LoanId == loan_model.LoanId);
            resp.Data = _mapper.Map<LoanDto>(l);

            return CreatedAtAction("GetLoan", new { id = resp.Data.LoanId }, resp);
        }

        // DELETE: api/Loans/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Loan>> DeleteLoan(int id)
        {
            var loan = await _context.Loan.FindAsync(id);
            if (loan == null)
            {
                return NotFound();
            }

            _context.Loan.Remove(loan);
            await _context.SaveChangesAsync();

            return loan;
        }

        private bool LoanExists(int id)
        {
            return _context.Loan.Any(e => e.LoanId == id);
        }
    }
}
