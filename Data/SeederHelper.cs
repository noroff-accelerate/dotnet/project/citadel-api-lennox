﻿using bookstore_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Data
{
    public class SeederHelper
    {
        public static ICollection<Book> SeedBooks()
        {
            ICollection<Book> books = new List<Book>()
            {
                new Book{ 
                    BookId = 1,
                    Title="A Clash of Kings", 
                    ISBN10="0006479898", 
                    ISBN13="9780006479895", 
                    Language="English", 
                    Format="Paperback", 
                    Length=928, 
                    Genre="Fantasy", 
                    Description="The second volume of A Song of Ice and Fire, the greatest fantasy epic of the modern age.",
                    Publisher = "HarperCollins Publishers", 
                    ReleaseDate = Convert.ToDateTime("01 Oct 1999"), 
                    QtyOnHand=10, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006479895.jpg"
                },
                new Book{
                    BookId = 2,
                    Title="A Feast for Crows", 
                    ISBN10="0006486126", 
                    ISBN13="9780006486121",
                    Language="English", 
                    Format="Paperback", 
                    Length=864, 
                    Genre="Fantasy",
                    Description="HBO's hit series A GAME OF THRONES is based on George R. R. Martin's internationally bestselling series A SONG OF ICE AND FIRE, the greatest fantasy epic of the modern age. A FEAST FOR CROWS is the fourth volume in the series.",
                    Publisher = "HarperCollins Publishers", 
                    ReleaseDate = Convert.ToDateTime("22 Aug 2011"),
                    QtyOnHand=10, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006486121.jpg"
                },
                new Book{
                    BookId = 3,
                    Title="A Storm Of Swords", 
                    ISBN10="055357342X", 
                    ISBN13="9780553573428",
                    Language="English", 
                    Format="Paperback", 
                    Length=1216, 
                    Genre="Fantasy",
                    Description="Here is the third volume in George R. R. Martin's magnificent cycle of novels that includes A Game of Thrones and A Clash of Kings.",
                    Publisher = "HarperCollins Publishers", 
                    ReleaseDate = Convert.ToDateTime("15 Sep 2005"),
                    QtyOnHand=10, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5535/9780553573428.jpg"
                },
                new Book{
                    BookId = 4,
                    Title="The Lord of the Rings : Boxed Set", 
                    ISBN10="0261102389", 
                    ISBN13="9780261102385",
                    Language="English", 
                    Format="Paperback", 
                    Length=1216 , 
                    Genre="Fantasy",
                    Description="Continuing the story of The Hobbit, this three-volume boxed set of Tolkien's epic masterpiece, The Lord of the Rings, features striking black covers based on Tolkien's own design, the definitive text, and three maps including a detailed map of Middle-earth.",
                    Publisher = "HarperCollins Publishers", 
                    ReleaseDate = Convert.ToDateTime("01 Apr 2008"), 
                    Edition = "2nd Revised ed",
                    QtyOnHand=5, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/2611/9780261102385.jpg"
                },
                new Book{
                    BookId = 5,
                    Title="The Hobbit and the Lord of the Rings", 
                    ISBN10="0544445783", 
                    ISBN13="9780544445789",
                    Language="English", 
                    Format="Paperback", 
                    Length=1504 , 
                    Genre="Fantasy",
                    Description="This four-volume, deluxe pocket boxed set contains J.R.R. Tolkien's epic masterworks The Hobbit and the three volumes of The Lord of the Rings (The Fellowship of the Ring, The Two Towers, and The Return of the King).",
                    Publisher = "HOUGHTON MIFFLIN", 
                    ReleaseDate = Convert.ToDateTime("21 Oct 2014"), 
                    Edition="Deluxe",
                    QtyOnHand=2, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5444/9780544445789.jpg"
                },
                new Book{
                    BookId = 6,
                    Title="The Shining", 
                    ISBN10="0307743659", 
                    ISBN13="9780307743657",
                    Language="English", 
                    Format="Paperback", 
                    Length=659, 
                    Genre="Horror",
                    Description="Before Doctor Sleep, there was The Shining, a classic of modern American horror from the undisputed master, Stephen King.",
                    Publisher = "Random House USA Inc", 
                    ReleaseDate = Convert.ToDateTime("07 Feb 2020"), 
                    Edition = "Reissue",
                    QtyOnHand=20, 
                    CoverImg="https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/3077/9780307743657.jpg"
                }
            };
            return books;
        }

        public static ICollection<Author> SeedAuthors()
        {
            ICollection<Author> authors = new List<Author>()
            {
                new Author{ 
                    AuthorId = 1,
                    Name = "George R. R. Martin"
                },
                new Author{
                    AuthorId = 2,
                    Name = "J. R. R. Tolkien"
                },
                new Author{
                    AuthorId = 3,
                    Name = "Stephen King"
                }
            };
            return authors;
        }

        public static ICollection<BookAuthor> SeedBookAuthors()
        {
            ICollection<BookAuthor> bookauthors = new List<BookAuthor>()
            {
                new BookAuthor{ BookId = 1, AuthorId = 1 },
                new BookAuthor{ BookId = 2, AuthorId = 1 },
                new BookAuthor{ BookId = 3, AuthorId = 1 },
                new BookAuthor{ BookId = 4, AuthorId = 2 },
                new BookAuthor{ BookId = 5, AuthorId = 2 },
                new BookAuthor{ BookId = 6, AuthorId = 3 }
            };
            return bookauthors;
        }

        public static ICollection<User> SeedUsers()
        {
            ICollection<User> users = new List<User>()
            {
                new User{ UserId = "example-user", UserName = "Example User", UserToken = "example-token" },
                new User{ UserId = "another-user", UserName = "Another User", UserToken = "another-token" }
            };
            return users;
        }
    }
}
