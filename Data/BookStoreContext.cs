﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
using bookstore_api.Data;

namespace bookstore_api.Models
{
    public class BookDbContext : DbContext
    {
        public BookDbContext(DbContextOptions<BookDbContext> options)
            : base(options)
        {
        }
        public DbSet<Author> Author { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<BookAuthor> BookAuthor { get; set; }
        public DbSet<Loan> Loan { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<BookLoan> BookLoan { get; set; }
        public DbSet<UserFavourite> UserFavourite { get; set; }

        // Create composite key for BookAuthor manually
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Set up composite keys
            modelBuilder.Entity<BookAuthor>().HasKey(ba => new { ba.AuthorId, ba.BookId });
            modelBuilder.Entity<BookLoan>().HasKey(bl => new { bl.LoanId, bl.BookId });
            modelBuilder.Entity<UserFavourite>().HasKey(uf => new { uf.UserId, uf.BookId });
            // Seed
            modelBuilder.Entity<Book>().HasData(SeederHelper.SeedBooks());
            modelBuilder.Entity<Author>().HasData(SeederHelper.SeedAuthors());
            modelBuilder.Entity<BookAuthor>().HasData(SeederHelper.SeedBookAuthors());
            modelBuilder.Entity<User>().HasData(SeederHelper.SeedUsers());
        }
    }
}
