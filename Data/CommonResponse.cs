﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.Models
{
    public class CommonResponse<T>
    {
        public T Data { get; set; }
        
        // If NULL -> Success
        public Error Error { get; set; }
    }
    public class Error
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
