# The Citadel minicase 
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Backend example for The Citadel minicase, specifications found
[here](https://gitlab.com/noroff-accelerate/cases/the-citadel/-/jobs/515841562/artifacts/file/The%20Citadel%20Case.pdf "Citadel minicase specifications").  

## Table of contents
- [Background](#background)
- [Deployment](#deployment)
- [Getting started](#getting-started) 
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#licence)

## Background
The current project serves as an example implementation of the backend requirements of the case. The backend, in this case, is a RESTful API created using ASP.NET Core 3.1 and the blank API template with no authentication or docker support added by default - these will be added manually to avoid many of the hidden steps Microsoft take. 

The endpoints are designed to meet the requirements set forth in the case specifications with adherence to current best practices. The project will make use of the [built in return methods](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.controllerbase?view=aspnetcore-3.1#methods "ControllerBase return methods") provided by ASP.NET Core's ControllerBase class. Examples when using a custom return object instead of the built in return methods are provided. This is done in order to maintain strict uniformity and adherence to the specifications instead of using the provided features. The choice of approach is up to the candidate as both are viable, this project simply presents more options.

## Deployment
The application is deployed using Docker containers on Azure. The deployment is automated by the use of CI/CD. Steps taken to create the automation can be found [here](https://gitlab.com/noroff-accelerate/dev-skills/hello-docker "HelloDocker example"). This means any changes pushed locally will be automatically Dockerised on GitLab and published to Azure. Some small changes are needed in the project to switch between development and publish, those changes will be detailed later.

## Getting started
Clone the repository locally
```
git clone git@gitlab.com:noroff-minicase-nlennox/bookstore-api.git
```
Open in Visual Studio.

Assuming you have SQL Server installed: run migrations using Package Manager console.
```
Update-Database
```
This will create a database in LocalDb, used for development. Seeded data coming soon™.

## Maintainers
Nicholas Lennox (@NicholasLennox)

## Contributing
If contributing, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## Licence 
MIT © 2020 Noroff Accelerate AS