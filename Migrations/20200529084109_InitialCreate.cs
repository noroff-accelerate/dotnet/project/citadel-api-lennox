﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace bookstore_api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Author",
                columns: table => new
                {
                    AuthorId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Author", x => x.AuthorId);
                });

            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    BookId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ISBN10 = table.Column<string>(maxLength: 13, nullable: true),
                    ISBN13 = table.Column<string>(maxLength: 17, nullable: true),
                    Title = table.Column<string>(maxLength: 200, nullable: true),
                    Genre = table.Column<string>(maxLength: 50, nullable: true),
                    Edition = table.Column<string>(nullable: true),
                    Keywords = table.Column<string>(nullable: true),
                    Publisher = table.Column<string>(maxLength: 50, nullable: true),
                    ReleaseDate = table.Column<DateTime>(nullable: true),
                    Format = table.Column<string>(maxLength: 50, nullable: true),
                    Language = table.Column<string>(maxLength: 50, nullable: true),
                    Length = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CoverImg = table.Column<string>(nullable: true),
                    QtyOnHand = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.BookId);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    UserToken = table.Column<string>(nullable: true),
                    ProfileImg = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "BookAuthor",
                columns: table => new
                {
                    BookId = table.Column<int>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookAuthor", x => new { x.AuthorId, x.BookId });
                    table.ForeignKey(
                        name: "FK_BookAuthor_Author_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Author",
                        principalColumn: "AuthorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookAuthor_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Loan",
                columns: table => new
                {
                    LoanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loan", x => x.LoanId);
                    table.ForeignKey(
                        name: "FK_Loan_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserFavourite",
                columns: table => new
                {
                    BookId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFavourite", x => new { x.UserId, x.BookId });
                    table.ForeignKey(
                        name: "FK_UserFavourite_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserFavourite_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BookLoan",
                columns: table => new
                {
                    BookId = table.Column<int>(nullable: false),
                    LoanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookLoan", x => new { x.LoanId, x.BookId });
                    table.ForeignKey(
                        name: "FK_BookLoan_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookLoan_Loan_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loan",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "AuthorId", "Name" },
                values: new object[,]
                {
                    { 1, "George R. R. Martin" },
                    { 2, "J. R. R. Tolkien" },
                    { 3, "Stephen King" }
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookId", "CoverImg", "Description", "Edition", "Format", "Genre", "ISBN10", "ISBN13", "Keywords", "Language", "Length", "Publisher", "QtyOnHand", "ReleaseDate", "Title" },
                values: new object[,]
                {
                    { 1, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006479895.jpg", "The second volume of A Song of Ice and Fire, the greatest fantasy epic of the modern age.", null, "Paperback", "Fantasy", "0006479898", "9780006479895", null, "English", 928, "HarperCollins Publishers", 10, new DateTime(1999, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "A Clash of Kings" },
                    { 2, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006486121.jpg", "HBO's hit series A GAME OF THRONES is based on George R. R. Martin's internationally bestselling series A SONG OF ICE AND FIRE, the greatest fantasy epic of the modern age. A FEAST FOR CROWS is the fourth volume in the series.", null, "Paperback", "Fantasy", "0006486126", "9780006486121", null, "English", 864, "HarperCollins Publishers", 10, new DateTime(2011, 8, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "A Feast for Crows" },
                    { 3, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5535/9780553573428.jpg", "Here is the third volume in George R. R. Martin's magnificent cycle of novels that includes A Game of Thrones and A Clash of Kings.", null, "Paperback", "Fantasy", "055357342X", "9780553573428", null, "English", 1216, "HarperCollins Publishers", 10, new DateTime(2005, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "A Storm Of Swords" },
                    { 4, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/2611/9780261102385.jpg", "Continuing the story of The Hobbit, this three-volume boxed set of Tolkien's epic masterpiece, The Lord of the Rings, features striking black covers based on Tolkien's own design, the definitive text, and three maps including a detailed map of Middle-earth.", "2nd Revised ed", "Paperback", "Fantasy", "0261102389", "9780261102385", null, "English", 1216, "HarperCollins Publishers", 5, new DateTime(2008, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Lord of the Rings : Boxed Set" },
                    { 5, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5444/9780544445789.jpg", "This four-volume, deluxe pocket boxed set contains J.R.R. Tolkien's epic masterworks The Hobbit and the three volumes of The Lord of the Rings (The Fellowship of the Ring, The Two Towers, and The Return of the King).", "Deluxe", "Paperback", "Fantasy", "0544445783", "9780544445789", null, "English", 1504, "HOUGHTON MIFFLIN", 2, new DateTime(2014, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Hobbit and the Lord of the Rings" },
                    { 6, "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/3077/9780307743657.jpg", "Before Doctor Sleep, there was The Shining, a classic of modern American horror from the undisputed master, Stephen King.", "Reissue", "Paperback", "Horror", "0307743659", "9780307743657", null, "English", 659, "Random House USA Inc", 20, new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Shining" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "UserId", "ProfileImg", "UserName", "UserToken" },
                values: new object[,]
                {
                    { "example-user", null, "Example User", "example-token" },
                    { "another-user", null, "Another User", "another-token" }
                });

            migrationBuilder.InsertData(
                table: "BookAuthor",
                columns: new[] { "AuthorId", "BookId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 4 },
                    { 2, 5 },
                    { 3, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookAuthor_BookId",
                table: "BookAuthor",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_BookLoan_BookId",
                table: "BookLoan",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Loan_UserId",
                table: "Loan",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFavourite_BookId",
                table: "UserFavourite",
                column: "BookId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookAuthor");

            migrationBuilder.DropTable(
                name: "BookLoan");

            migrationBuilder.DropTable(
                name: "UserFavourite");

            migrationBuilder.DropTable(
                name: "Author");

            migrationBuilder.DropTable(
                name: "Loan");

            migrationBuilder.DropTable(
                name: "Book");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
