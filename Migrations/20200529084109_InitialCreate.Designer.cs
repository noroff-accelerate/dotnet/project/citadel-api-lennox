﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using bookstore_api.Models;

namespace bookstore_api.Migrations
{
    [DbContext(typeof(BookDbContext))]
    [Migration("20200529084109_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("bookstore_api.Models.Author", b =>
                {
                    b.Property<int>("AuthorId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("AuthorId");

                    b.ToTable("Author");

                    b.HasData(
                        new
                        {
                            AuthorId = 1,
                            Name = "George R. R. Martin"
                        },
                        new
                        {
                            AuthorId = 2,
                            Name = "J. R. R. Tolkien"
                        },
                        new
                        {
                            AuthorId = 3,
                            Name = "Stephen King"
                        });
                });

            modelBuilder.Entity("bookstore_api.Models.Book", b =>
                {
                    b.Property<int>("BookId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CoverImg")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Edition")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Format")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Genre")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("ISBN10")
                        .HasColumnType("nvarchar(13)")
                        .HasMaxLength(13);

                    b.Property<string>("ISBN13")
                        .HasColumnType("nvarchar(17)")
                        .HasMaxLength(17);

                    b.Property<string>("Keywords")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Language")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<int?>("Length")
                        .HasColumnType("int");

                    b.Property<string>("Publisher")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<int>("QtyOnHand")
                        .HasColumnType("int");

                    b.Property<DateTime?>("ReleaseDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(200)")
                        .HasMaxLength(200);

                    b.HasKey("BookId");

                    b.ToTable("Book");

                    b.HasData(
                        new
                        {
                            BookId = 1,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006479895.jpg",
                            Description = "The second volume of A Song of Ice and Fire, the greatest fantasy epic of the modern age.",
                            Format = "Paperback",
                            Genre = "Fantasy",
                            ISBN10 = "0006479898",
                            ISBN13 = "9780006479895",
                            Language = "English",
                            Length = 928,
                            Publisher = "HarperCollins Publishers",
                            QtyOnHand = 10,
                            ReleaseDate = new DateTime(1999, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "A Clash of Kings"
                        },
                        new
                        {
                            BookId = 2,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006486121.jpg",
                            Description = "HBO's hit series A GAME OF THRONES is based on George R. R. Martin's internationally bestselling series A SONG OF ICE AND FIRE, the greatest fantasy epic of the modern age. A FEAST FOR CROWS is the fourth volume in the series.",
                            Format = "Paperback",
                            Genre = "Fantasy",
                            ISBN10 = "0006486126",
                            ISBN13 = "9780006486121",
                            Language = "English",
                            Length = 864,
                            Publisher = "HarperCollins Publishers",
                            QtyOnHand = 10,
                            ReleaseDate = new DateTime(2011, 8, 22, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "A Feast for Crows"
                        },
                        new
                        {
                            BookId = 3,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5535/9780553573428.jpg",
                            Description = "Here is the third volume in George R. R. Martin's magnificent cycle of novels that includes A Game of Thrones and A Clash of Kings.",
                            Format = "Paperback",
                            Genre = "Fantasy",
                            ISBN10 = "055357342X",
                            ISBN13 = "9780553573428",
                            Language = "English",
                            Length = 1216,
                            Publisher = "HarperCollins Publishers",
                            QtyOnHand = 10,
                            ReleaseDate = new DateTime(2005, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "A Storm Of Swords"
                        },
                        new
                        {
                            BookId = 4,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/2611/9780261102385.jpg",
                            Description = "Continuing the story of The Hobbit, this three-volume boxed set of Tolkien's epic masterpiece, The Lord of the Rings, features striking black covers based on Tolkien's own design, the definitive text, and three maps including a detailed map of Middle-earth.",
                            Edition = "2nd Revised ed",
                            Format = "Paperback",
                            Genre = "Fantasy",
                            ISBN10 = "0261102389",
                            ISBN13 = "9780261102385",
                            Language = "English",
                            Length = 1216,
                            Publisher = "HarperCollins Publishers",
                            QtyOnHand = 5,
                            ReleaseDate = new DateTime(2008, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Lord of the Rings : Boxed Set"
                        },
                        new
                        {
                            BookId = 5,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5444/9780544445789.jpg",
                            Description = "This four-volume, deluxe pocket boxed set contains J.R.R. Tolkien's epic masterworks The Hobbit and the three volumes of The Lord of the Rings (The Fellowship of the Ring, The Two Towers, and The Return of the King).",
                            Edition = "Deluxe",
                            Format = "Paperback",
                            Genre = "Fantasy",
                            ISBN10 = "0544445783",
                            ISBN13 = "9780544445789",
                            Language = "English",
                            Length = 1504,
                            Publisher = "HOUGHTON MIFFLIN",
                            QtyOnHand = 2,
                            ReleaseDate = new DateTime(2014, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Hobbit and the Lord of the Rings"
                        },
                        new
                        {
                            BookId = 6,
                            CoverImg = "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/3077/9780307743657.jpg",
                            Description = "Before Doctor Sleep, there was The Shining, a classic of modern American horror from the undisputed master, Stephen King.",
                            Edition = "Reissue",
                            Format = "Paperback",
                            Genre = "Horror",
                            ISBN10 = "0307743659",
                            ISBN13 = "9780307743657",
                            Language = "English",
                            Length = 659,
                            Publisher = "Random House USA Inc",
                            QtyOnHand = 20,
                            ReleaseDate = new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Shining"
                        });
                });

            modelBuilder.Entity("bookstore_api.Models.BookAuthor", b =>
                {
                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<int>("BookId")
                        .HasColumnType("int");

                    b.HasKey("AuthorId", "BookId");

                    b.HasIndex("BookId");

                    b.ToTable("BookAuthor");

                    b.HasData(
                        new
                        {
                            AuthorId = 1,
                            BookId = 1
                        },
                        new
                        {
                            AuthorId = 1,
                            BookId = 2
                        },
                        new
                        {
                            AuthorId = 1,
                            BookId = 3
                        },
                        new
                        {
                            AuthorId = 2,
                            BookId = 4
                        },
                        new
                        {
                            AuthorId = 2,
                            BookId = 5
                        },
                        new
                        {
                            AuthorId = 3,
                            BookId = 6
                        });
                });

            modelBuilder.Entity("bookstore_api.Models.BookLoan", b =>
                {
                    b.Property<int>("LoanId")
                        .HasColumnType("int");

                    b.Property<int>("BookId")
                        .HasColumnType("int");

                    b.HasKey("LoanId", "BookId");

                    b.HasIndex("BookId");

                    b.ToTable("BookLoan");
                });

            modelBuilder.Entity("bookstore_api.Models.Loan", b =>
                {
                    b.Property<int>("LoanId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoanId");

                    b.HasIndex("UserId");

                    b.ToTable("Loan");
                });

            modelBuilder.Entity("bookstore_api.Models.User", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProfileImg")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserToken")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId");

                    b.ToTable("User");

                    b.HasData(
                        new
                        {
                            UserId = "example-user",
                            UserName = "Example User",
                            UserToken = "example-token"
                        },
                        new
                        {
                            UserId = "another-user",
                            UserName = "Another User",
                            UserToken = "another-token"
                        });
                });

            modelBuilder.Entity("bookstore_api.Models.UserFavourite", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("BookId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnType("datetime2");

                    b.HasKey("UserId", "BookId");

                    b.HasIndex("BookId");

                    b.ToTable("UserFavourite");
                });

            modelBuilder.Entity("bookstore_api.Models.BookAuthor", b =>
                {
                    b.HasOne("bookstore_api.Models.Author", "Author")
                        .WithMany("BookAuthors")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("bookstore_api.Models.Book", "Book")
                        .WithMany("BookAuthors")
                        .HasForeignKey("BookId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("bookstore_api.Models.BookLoan", b =>
                {
                    b.HasOne("bookstore_api.Models.Book", "Book")
                        .WithMany("BookLoans")
                        .HasForeignKey("BookId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("bookstore_api.Models.Loan", "Loan")
                        .WithMany("BookLoans")
                        .HasForeignKey("LoanId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("bookstore_api.Models.Loan", b =>
                {
                    b.HasOne("bookstore_api.Models.User", "User")
                        .WithMany("Loans")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("bookstore_api.Models.UserFavourite", b =>
                {
                    b.HasOne("bookstore_api.Models.Book", "Book")
                        .WithMany("UserFavourites")
                        .HasForeignKey("BookId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("bookstore_api.Models.User", "User")
                        .WithMany("UserFavourites")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
