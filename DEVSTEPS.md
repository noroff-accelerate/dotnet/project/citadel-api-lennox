# Development steps
This file serves as a step-by-step documentation of everything done to create the BookstoreAPI applicaiton. It will document the resources used and the thought process behind each descision.

## Table of contents
- [Creation](#creation)
- [Docker](#docker)
- [Automatic publishing](#automatic-publishing)
- [Database](#database)
- [Models](#models)
- [ApplicationDbContext](#applicationdbcontext)
- [Migrations](#migrations)
- [Endpoints](#endpoints)
- [Data Transfer Objects](#data-transfer-objects)
- [Controllers](#conrtrollers)
- [Authentication](#authentication)

## Creation
Open Visual Studio and create new Web Application using the API template in the folder where your repository is located.
```
File > New > Project > ASP.NET Core Web Application > API
```
Do **not** add `Authentication` or `Docker Support`.

Run the application to check if it is working as intended.

## Docker
Before any coding is done, we want to set up Docker for the application to publish it locally. This involves adding two files; `Dockerfile` and `.dockerignore` and creating a Docker image using [Docker Hub](https://hub.docker.com/). We will automate this process at a later stage with CI/DI to create and publish Docker images to Azure. This is to simply get a feel for publishing with Docker manually.

The Docker local publish setup is done by following [this](https://dotnetplaybook.com/deploy-a-net-core-api-with-docker/) tutorial. In this tutorial you learn how to create the `Dockerfile` and `.dockerignore` files, create images using Docker Hub, run those images locally, and finally publish to Docker Hub.

## Automatic publishing
Ja nee

## Database
This section will create a database scheme based on the requirements set forth in the specification. It will mainly focus on the entities and thier relationships, the finer details are done when discussing the models. 

`Books` are the main focus of the application and thus are central to its design. Books have a relationship with authors where a book can have many authors and an author can have many books. This indicates a `many-to-many` relationship between `Books` and `Authors`.

`Users` have `Profiles`, the user information is managed off site with [Keycloak](https://www.keycloak.org/). This means that the only user information we store is a reference to the user on keycloak. As a result, only a `Profile` entity is saved in our database - this is to allow some business logic including our users to be done.

`Loans` are the main functionality of the system. A `Loan` has a relationship with a `Book` and a `Profile`. This is due to the need to track which user is loaning which books. 

### Entity Relationship Diagram (ERD)
PrettyPicture.jpeg
## Models
Models are C# classes that represent our database tables. The model is the **M** in MVC. Using models allows us to make use of Entity Framework Core - a huge time saver - to handle our database interactions. In line with conventions for MVC, the models for an application should be placed in a `Models` folder. To create a new folder, from Solution Explorer:
```
Right-click the project > Add > New Folder
```
Name the folder *Models*. The next step is adding our model classes. You do this by:
```
Right-click Models folder > Add > Class
```
We need to create models that accuratly model our database schema, with relationships and validation. Here is a [refresher on Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/).

The following sections will list out each of the models needed as well as the various relationships. This is taken from the ERD drawn up beforehand - an important step. Validation takes the form of [data annotations](https://docs.microsoft.com/en-us/ef/ef6/modeling/code-first/data-annotations) to create richer models.
```c#
using System.ComponentModel.DataAnnotations;
```
We will be using convention to create relationships between our tables with navigation properties. This can also be done wihtout navigation properties by configuring the DbContext; informaiton on relationships in EF Core can be found [here](https://docs.microsoft.com/en-us/ef/core/modeling/relationships?tabs=fluent-api%2Cfluent-api-simple-key%2Csimple-key).
### Book
The primary key for the book is going to be a autoincremented integer. A book has two [ISBN](https://en.wikipedia.org/wiki/International_Standard_Book_Number) numbers.
> The ISBN is ten digits long if assigned before 2007, and thirteen digits long if assigned on or after 1 January 2007.

This results in ISBN and/or ISNB13 identifiers. Futhermore, the digits can be broken up with `-` to seperate out the [various ranges](https://www.isbn-international.org/range_file_generation). 
> [EAN.UCC prefix]-[Registration Group element]-[Registrant element]-[Publication element]-[Check-digit]

The `Title` of the book should not be longer than [200 characters](https://www.goodreads.com/list/show/2667.Books_with_Really_Long_Titles), although there is a book with a [5,820 character](https://atkinsbookshelf.wordpress.com/2016/07/09/what-is-the-longest-book-title-in-the-world/) title - this is an extreme outlier and wont be catered for due to storage size for fields that large. 

The book's `Genre` gives it a aspect that can be used to categorise it. A genre should not be longer than [50 characters](https://en.wikipedia.org/wiki/List_of_writing_genres). This is a very generous size as most genres in writing are less than 20 characters in length. **Note:** no endpoints required for genre, so it should be fine to store it as plain text and not make a seperate table.

A book has a `Publisher`, there are many publishers depending on [language](https://en.wikipedia.org/wiki/Lists_of_publishing_companies). Once again, a very generous estimation of size should be a maximim of 50 characters, whereas most are [far smaller](https://en.wikipedia.org/wiki/List_of_English-language_book_publishing_companies). **Note:** No requirements around publishers stated in docs, therefore simple plaintext storage is used for publishers.

The `?` next to the `string` type indicates that this field should be nullable. To avoid warnings in Visual Studio, wrap all areas where nullable types are defined in `#nullable`. 

Books have a `PublicationDate`, which is when the book was published. The `[DataType(DataType.Date)]` attribute indicates that we only are interested in the date portion and want to exclude the time portion. More information on the `DataType` enemerator can be found [here](https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.datatype?view=netcore-3.1).

A book's `Format` indicates whether is is a paperback, hardcover, e-book, ect. This should not be longer than 50 characters.

A book is written in a specific `Language`. This field should have a maximum of 50 characters.

A book has a `Length`, which is the number of pages in the book. 

A book's `Description` give the user an indication for what the book is about. It can be a plain summary, or include some reviews. For this reason, we dont want to limit the user with this field and allow it to be the maximum size the database can handle. 

A book has a `CoverImg`, which is a picture of the book itself. This image will not be stored on the server, in its place will be a URL to the image stored elsewhere. This is to limit the size of the database. The use of `[DataType(DataType.ImageUrl)]` indicates that it has the format of an image url. This is used for validation and to help prevent possible SQL Injection. 

A book's relationship with `Authors` is handled via a linking table. This means there needs to be a navigation property for this for Entity Framework to be able to navigate. This navigation is done with the `BookAuthor` linking table as EF does not automatically handle `many-to-many` relationships. 

**NOTE:** Need to include logic for availablity for loans. My thoughts are to keep a QtyOnHand and use some caluculation to see how many are on loan with a ISBN number and calculate if the book is available. This wont apply to digital versions - there should be some kind of flag so we dont have to set qty to some rediculous number - or maybe we should.

**NOTE:** No user review system is described or planned in the specs.
```c#
public class Book
{
    [Key]
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int BookId { get; set; }

    [MaxLength(13)]
    public string ISBN10 { get; set; }

    [MaxLength(17)]
    public string ISBN13 { get; set; }

    [MaxLength(200)]
    public string Title { get; set; }

    [MaxLength(50)]
    public string Genre { get; set; }

#nullable enable
    [MaxLength(50)]
    public string? Publisher { get; set; }

    [DataType(DataType.Date)]
    public DateTime? ReleaseDate { get; set; }

    [MaxLength(50)]
    public string? Format { get; set; }

    [MaxLength(50)]
    public string? Language { get; set; }

    public int? Length { get; set; }

    public string? Description { get; set; }

    [DataType(DataType.ImageUrl)]
    public string? CoverImg { get; set; }
#nullable disable

    // Possible additions for loans logic
    public int QtyOnHand { get; set; }

    /* 
    This is a navigation property to BookAuthor, 
    the linking table between Book and Author. 
    */
    public ICollection<BookAuthor> BookAuthors { get; set; }
}
```
### Author
The unique identifier for an `Author` is simply an autoincremented integer. Authors often use pen names when they [author novels](https://en.wikipedia.org/wiki/Pen_name), they also could use their full name. Some more famous authors with pen names include: 
- George R. R. Martin -> George Raymond Richard Martin
- J. R. R. Tolkien -> John Ronald Reuel Tolkien 
- J. K. Rowling -> Joanne Rowling 

This makes splitting their names into parts introduces uneccessary problems. For this reason the `Name` is stored and will either be the author's real name or their pen name; no extra information is stored. There is also a navigation property present to allow Entity Framework to link `Author` and `BookAuthor` by convention. 
```c#
public class Author
{
    [Key]
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int AuthorId { get; set; }

    [MaxLength(50)]
    [Required]
    public string Name { get; set; }

    /* 
    This is a navigation property to BookAuthor, 
    the linking table between Book and Author.
    */
    public ICollection<BookAuthor> BookAuthors { get; set; }
}
```
### BookAuthor
Simply a model for the linking table between `Book` and `Author`. No information is stored other that the linked foreign keys, which are used in conjunction to create a unique composite primary key. Navigation properties for `Book` and `Author` are present for convention. Due to the primary key being a composite key, convention cannot be used to create it. This needs to be done manually, using the [FluentAPI](), the `BookDbContext` class demonstrates this configuraiton - detailed at a later stage.
```c#
public class BookAuthor
{
    [Key]
    [Required]
    public int BookId { get; set; }
    // Navigation property to Book
    public Book Book { get; set; }
    
    [Key]
    [Required]
    public int AuthorId { get; set; }
    // Navigation property to Author
    public Author Author { get; set; }
}
```
### Profile
As all the user information is stored in Keycloak, we only keep a reference to that user as our primary key. I am not 100% sure about the data format just yet so to be safe, its going to be a `string`. As well as a token reference to Keycloak.

In addition to this, we need to store their profile picture (optional, or set a default - not decided). Once again, we are not storing the actual image, but a link to an externally stored image. A navigation property to `Loan` is provided to infer the relationship by convention. 
```c#
public class Profile
{
    [Key]
    [Required]
    public string ProfileId { get; set; }

    // Reference for keycloak
    public string UserToken { get; set; }

#nullable enable
    [DataType(DataType.ImageUrl)]
    public string? ProfileImg { get; set; }
#nullable disable

    // Navigation property to Loan
    public ICollection<Loan> Loans { get; set; }
}
```
### Loan
Users of the system can request books to loan. A loan's unique identifier is an autoincremented integer. A loan needs a reference to what book is on loan and to who it is loaned. This means we need to have `ProfileId` from `Profile` and `ISBN` from `Book`. 

We need to keep track of when the loan started and its expected end. A status needs to be kept to see if the book is overdue; i.e. status is "loaned" after the expected return date. The deafult status for a new loan is "loaned".
```c#
public class Loan
{
    [Key]
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int LoanID { get; set; }

    [Required]
    public int BookId { get; set; }
    // Navigation property to Book class
    public Book Book { get; set; }
    
    [Required]
    public int ProfileId { get; set; }
    // Navigation property to Author class
    public Profile Profile { get; set; }

    [DataType(DataType.Date)]
    public DateTime StartDate { get; set; }

    [DataType(DataType.Date)]
    public DateTime EndDate { get; set; }

    [MaxLength(10)]
    public string Status { get; set; }
}
```
## ApplicationDbContext
The database context (`ApplicationDbContext`) is the main class that coordinates Entity Framework functionality for a data model. This class is created by deriving from the `Microsoft.EntityFrameworkCore.DbContext` class.

We list all our entities here and their realtionships are inferred from the various navigation properties set up in the models. If we did not setup any convention, we could manually configure the realtionships here. This section will demostrate both scenarios. Data can be seeded into a database in this class as well.

Firslty, we need to add the relevent packages to our application. Through the `Nuget Package Manager` add the following packages:

- `Microsoft.EntityFrameworkCore.SqlServer`
- `Microsoft.EntityFrameworkCore.Tools`

The `ApplicationDbContext` class can now be created and used to configure EF Core. This class can either be stored in the `Models` folder, or a `Data` folder. Recall, the class inherits from `DbContext` and overrides the constructor. Then we need to register all the `Models` we need to be stored in our database. In our case, the class is called `BookDbContext`.
```c#
...
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
...

public class BookDbContext : DbContext
{
    public BookDbContext(
        DbContextOptions<BookDbContext> options
    ): base(options)
    {
    }
    /* 
    These are tables that will appear in the db.
    Their relationships are inferred from the convention
    used with navigation properties. 
    */
    public DbSet<Author> Author { get; set; }
    public DbSet<Book> Book { get; set; }
    public DbSet<BookAuthor> BookAuthor { get; set; }
    public DbSet<Loan> Loan { get; set; }
    public DbSet<Profile> Profile { get; set; }

    // Create composite key for BookAuthor manually
    protected override void OnModelCreating(
        ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BookAuthor>().HasKey(
            ba => new { ba.AuthorId, ba.BookId });
    }
}
```
Once the `DbContext` class has been created, the application needs to be told that we are using EF. This is done in the `Startup.cs` file in the `ConfigureServices` method. In this case, SQL Server will be used as the database provider and the connection string stored in a configuration file. 
```c#
...
using Microsoft.EntityFrameworkCore;
using bookstore_api.Models;
...
public void ConfigureServices(IServiceCollection services)
{
    services.AddControllers();
    // Configure support for EF
    services.AddDbContext<BookDbContext>(
        opt => opt.UseSqlServer(
            Configuration.GetConnectionString("Default")
        )
    );
}
```
`Configuration.GetConnectionString("Default")` tells the applicaiton to go look in a configuration file for  a property called "ConnectionStrings" and return the one labled "Default". The configuration file in question is the `appsettings.json` file, documentation about configuring connection strings in ASP.NET Core can be found [here](https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-strings#aspnet-core). At this point in the development stage, the `appsettings.json` file is in the following state:
```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "Default": "Server=(localdb)\\mssqllocaldb;
    Database=Bookstore;Trusted_Connection=True;
    MultipleActiveResultSets=true"
  }
}
```
For this example, a `localdb` is used, this means that Visual Studio will work with SQL Server to create a database instance in the project file. This database is called `Bookstore`. More information on LocalDb can be found [here](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/working-with-sql?view=aspnetcore-3.1&tabs=visual-studio).

For production builds, the connection string is stored as either a `secret` or `environment variable`, information on secret managers and environment variables can be found [here](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1#security-and-secret-manager). The connection string is moved to an appropriate location at a later stage, for now it is kept easily accessable during development.
## Migrations
At this point in time, there is no database created. This problem is solved with migrations. Migrations use the `Models` and the `ApplicationDbContext` class created to map to a SQL database in this case. To include support for migrations you need to add the `Microsoft.EntityFrameworkCore.Design` package to your solution through Nuget. 

Once the relevant package is added, open the PM cconsole and type `add-migration InitialCreate`. This will create a `Migrations` folder with a snapshot of the context and the newly created migration added. To run the migration, run the `update-database` command in the PM console. This will create the database specified in the `Default` connection string. Migrations are used to keep the dbcontext in sync with the database. Any changes to the context or models require a new migration to be done. Mirgrations can also be rolled back or the database can be set to a specific migration, more informaiton can be found [here](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli).

Once the `update-database` command has been run successfully, the `Bookstore` database can be viewed from inside Visual Studio. 
```
View > SQL Server Object Explorer > SQL Server > (localdb)\MSSQLLocalDB > Databases
```
An example of this structure is illustrated below.

![image info](./documentation-images/sql-server-object-explorer.png)

Best practices for naming in SQL Server is discussed [here](https://stackoverflow.com/a/3593804). The largest ones to follow are the table and column naming conventions. This requires meaningful names for both tables and columns, no underscore usage, singular names for tables, and the limited use of abbreviations. 

## Endpoints
This section describes the endpoint set forth in the [specificaitons](https://gitlab.com/noroff-accelerate/cases/the-citadel/-/jobs/515841562/artifacts/file/The%20Citadel%20Case.pdf). The endpoints are relisted here for ease of access, as they inform the descisions for subsequent sections.

Looking at Section 3.3 of the specifications, the requirements for the endpoints are described. Some basic, non-specific, requirements are: no query parameters other than search, and login should return a Jwt with user information inside. The user token is required to access all other endpoints as they will require authorization. The following subsections detail each endpoint required, grouped by category.

### Login
```
POST /login
```

### User
```
GET /user
```

```
POST /user
```

```
GET /user/:user_id
```

```
PATCH /user/:user_id
```

```
DELETE /user/:user_id
```

```
POST /user/:user_id/update_password
```

```
GET /user/:user_id/books
```

```
GET /user/user_id/calendar
```

### Profile
```
POST /profile
```

```
GET /profile/profile_id
```

```
PATCH /profile/profile_id
```

```
DELETE /profile/profile_id
```

### Books
```
POST /book
```

```
GET /book/:book_id
```

```
PATCH /book/:book_id
```

```
DELETE /book/:book_id
```

### Authors
```
POST /author
```

```
GET /author/author_id
```

```
PATCH /author/author_id
```

```
DELETE /author/author_id
```

### Loans
**Note:** Seems to be no documentaiton about loans outside of users getting books they have on loan. 

### Default responses
This subsection details what the application should respond with for various actions.
- Successful requests should return either: *200 OK*, *201 Created* or *204 No Content*.
- Instances where a record is created should return *201 Created* with the location of the newly created resource.
- When input validation fails, return *400 Bad Request*.
- When an unauthorized user is trying to access protected endpoints, return *401 Unauthorized*. This needs to happen even if the end point doesnt exist. 
- When the user is authenticated but not authorized, a *403 Forbidden* should be returned. 
- When requests are made to unkown endpoints, a *404 Not Found* should be returned. 
- When the server enters an error, an *500 Internal Server Error* is returned. Messages should be included during development, in production that should be omited. 

### Common response object
Best practice is to return a wrapper object, called a common response object, to the client with every service call. Additional information can be added to the returning result, like a success or exception message. The front end is able to react to this additional information and read the actual data with the help of HTTP interceptors. Generics can be used in the class design to cater for any type of object. This object is placed in the `Models` folder. **Note:** Where else should I palce this? - feels weird being in models. 
```c#
public class CommonResponse<T>
{
    public T Data { get; set; }

    public Error Error { get; set; }
}
public class Error 
{
    public int Status { get; set; }
    public string Message { get; set; }
}
```
With this common repsonse object, the returned data is wrapped into `data` and if it was successful, error is `null`. This means that the client simply needs to evaluate the truthy `if(error)` to handle errors. 

If the condition returns true, the error exists; with the status code and message wrapped inside. If the condiditon returns false, there is no error - meaning it was a success. The Http response codes are included in the header, and for a `201 Created` response, the `location` header is used to provide a URI to the newly created resource.  

This will be the common object used as a return type in all action methods in the controllers. The generic `T` will dictate what type of object is wrapped inside. 

## Data Transfer Objects
It is not a recommeded practice to return entities - classes that model our database - from Web APIs. This is for a number of reasons, namely to:
- Remove circular references.
- Hide particular properties that clients are not supposed to view.
- Omit some properties in order to reduce payload size.
- Flatten object graphs that contain nested objects, to make them more convenient for clients.
- Avoid "over-posting" vulnerabilities.
- Decouple your service layer from your database layer.

A data transfer object (DTO) is an object that defines how the data will be sent over the network. What this means is that you create a intermediary between your internal data structure and what you expose to the client. 

In the points listed above, the last point is one of the most important ones. What that point means is that you want to separate your exposed data from your internal data structure; this allows the internal data structure to change without breaking the clients using the API. This ensures the code written is loosely coupled - inline with best practices.

It is important to note the terminology surrounding DTOs, especially in the context of ASP.NET Core MVC web applications, DTOs are often reffered to as ViewModels. 

To add DTOs to the applicaiton, a new folder, named DTOs, needs to be created to contain all the DTOs.
```
Right click project > Add > New Folder
```
Now a folder for each entity needs to be created, in the current context the following folders need to be created inside the DTOs folder:
- `Author`
- `Book`
- `Loan`

The relevent DTO classes will go in their respective folders. The DTOs are created to meet each of the endpoint requirements. The following subsections detail the various DTOs needed, seperated into categories. 

### Author
The requirements for Author are simple CRUD functions. This means the following DTOs are needed:
- `AuthorCreateDto`
- `AuthorReadDto`

The `AuthorCreateDto` is reused for PATCH/PUT functionality. When an author is created, nothing but their name is required. The Dto needs to reflect the validation required by the model class, the code snippet below shows the `AuthorCreateDto`.
```c#
public class AuthorCreateDto
{
    // Either full name or pen name (if they use pen name)
    [Required]
    [MaxLength(50)]
    public string Name { get; set; }
}
```
When an author is read for display purposes, the Id is needed. No validation needs to be performed on a read; as this is information coming from the database which is in the correct format. The `AuthorDto` is shown in the code snippet below.
```c#
public class AuthorDto
{
    public int AuthorId { get; set; }
    public string Name { get; set; }
}
```
### Book
- `BookCreateDto`
- `BookDto`
- `BookReadGalleryDto`
- `BookReadShortDto`

The `BookCreateDto` is reused for PATCH/PUT functionality. There are two versions of reading the book, the one is for displaying in a grid/list (Gallery) and the other includes all the other informaiton needed to show a full profile (Full). 

As was the case with authors; when creating a book object, the Id is not supplied. The properties need to be validated as they are in the model classes. The code snippet below shows the `BookCreateDto`.
```c#
public class BookCreateDto
{
    [MaxLength(13)]
    public string ISBN10 { get; set; }
    [MaxLength(200)]
    public string Title { get; set; }
    [MaxLength(50)]
    public string Genre { get; set; }
#nullable enable
    [MaxLength(17)]
    public string? ISBN13 { get; set; }
    [MaxLength(50)]
    public string? Publisher { get; set; }
    [DataType(DataType.Date)]
    public DateTime? ReleaseDate { get; set; }
    [MaxLength(50)]
    public string? Format { get; set; }
    [MaxLength(50)]
    public string? Language { get; set; }
    public int? Length { get; set; }
    public string? Description { get; set; }
    [DataType(DataType.ImageUrl)]
    public string? CoverImg { get; set; }
#nullable disable
    public int QtyOnHand { get; set; }
    public List<int> Authors { get; set; }
}
```
The authors of the book is simply passes as a list of integers. This means that the Author object is abstracted into a simple list of the AuthorIds - no more than this is needed and thus passing an entire author object is too much. If a new author needs to be added it can be done asyncronously by the front end before the book is added.

Reading a full book requires all the information without validation - for the same reason as with authors. The code snippet below shows the `BookDto`.
```c#
public class BookDto
{
    public int BookId { get; set; }
    public string ISBN10 { get; set; }
    public string ISBN13 { get; set; }
    public string Title { get; set; }
    public string Genre { get; set; }
    public string Publisher { get; set; }
    public DateTime ReleaseDate { get; set; }
    public string Format { get; set; }
    public string Language { get; set; }
    public int Length { get; set; }
    public string Description { get; set; }
    public string CoverImg { get; set; }
    public int QtyOnHand { get; set; }
    public List<AuthorReadDto> Authors { get; set; }
}
```
When displaying books in a gallery or grid format, not much informaiton is needed. For this reason the `BookReadGalleryDto` is a cut down version of the full read. The code snippet below shows this Dto.
```c#
public class BookReadGalleryDto
{
    public int BookId { get; set; }
    public string Title { get; set; }
    public string Genre { get; set; }
    public string CoverImg { get; set; }
    public int QtyOnHand { get; set; }
    public List<AuthorReadDto> Authors { get; set; }
}
```

When books are needed for looking at loans, their titles are needed with an Id to navigate to the resource. A `BookReadShortDto` is created for this purpose and is shown in the code snippet below.
```c#
public class BookReadShortDto
{
    public int BookId { get; set; }
    public string Title { get; set; }
}
```

### Loan
**NOTE:** Pretty sure I have the Loan relationship wrong - as it stands only one book can be taken out per loan. ATM I can only think of a solution that involves a new linking table. 
- `LoanCreateDto`
- `LoanReadDto`

The `LoanCreateDto` is reused for PATCH/PUT functionality.
When creating a loan, the Id is not required. The navigation properties are also redundant. The validation is still needed, for the same reasons as books and authors. The code snippet below shows the `LoanCreateDto`.
```c#
public class LoanCreateDto
{    
    [MaxLength(17)]
    [Required]
    public int BookId { get; set; }    
    [Required]
    public string ProfileId { get; set; }
    [DataType(DataType.Date)]
    public DateTime StartDate { get; set; }
    [DataType(DataType.Date)]
    public DateTime EndDate { get; set; }
    [MaxLength(10)]
    public string Status { get; set; }
}
```
When reading a loan, we replace the Ids with the actual titles and names, but still need to keep the Id for links. The ReadShort dtos are made use of here. The code snippet below shows the `LoanReadDto`.
```c#
public class LoanReadDto
{
    public int LoanId { get; set; }
    /* Should be a collection as one loan 
       can have many books - will fix */
    public BookReadShortDto Book { get; set; }
    /* ProfileId kept as we currently 
       dont store the name - so no Dto */
    public string ProfileId { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Status { get; set; }
}
```
### User/Profile
**NOTE:** With keycloak, unsure how exactly to handle this all.

### Automapper
When using Dtos, there needs to be a conversion between model classes and Dtos. This can be done by invoking anonymous functions to manually map fields over. The code snippet below illustrates this based on Microsofts [tutorial on creating DTOs](https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5).
```c#
[ResponseType(typeof(BookDto))]
public async Task<IHttpActionResult> PostBook(Book book)
{
    if (!ModelState.IsValid)
    {
        return BadRequest(ModelState);
    }

    db.Books.Add(book);
    await db.SaveChangesAsync();
    db.Entry(book).Reference(x => x.Author).Load();

    // Map to DTO
    var dto = new BookDto()
    {
        Id = book.Id,
        Title = book.Title,
        AuthorName = book.Author.Name
    };

    return CreatedAtRoute("DefaultApi", 
        new { id = book.Id }, dto);
}
```
Although this is relatively simple for smaller objects, this becomes cumbersome and is highly error prone. The approach to a solution on this involves the [Automapper](http://automapper.org/) package.

The first [step](https://code-maze.com/automapper-net-core/) is to install the Automapper Nuget package. Using the PM console, write the following command:
```c#
Install-Package AutoMapper.Extensions.Microsoft.DependencyInjection
```

After installing the required package, the next step is to configure the services. In `startup.cs`, add the Automapper service:
```c#
public void ConfigureServices(IServiceCollection services)
{
    services.AddAutoMapper(typeof(Startup));
```

Now, mapping can be created. An example of a mapping done in this project is from the `Author` model class to the `AuthorCreateDto`. 
```c#
// From this

public class Author
{
    [Key]
    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int AuthorId { get; set; }

    [MaxLength(50)]
    [Required]
    public string Name { get; set; }
    
    public ICollection<BookAuthor> BookAuthors { get; set; }
}

// To this

public class AuthorCreateDto
{
    [Required]
    [MaxLength(50)]
    public string Name { get; set; }
}
```

This is a very simple example, but its easy to illustrate what needs to be done to create automatic mappings. 

A good way to organize our mapping configurations is with `Profiles`. Classes need to be created that inherit from the `Profile` class and be configured in the constructor. In this case, a `AuthorProfile` class is created in a folder called `Profiles`.
```c#
public class AuthorProfile : Profile
{
    public AuthorProfile()
    {
        CreateMap<AuthorCreateDto, Author>();
    }
}
```
`AuthorProfile` class creates the mapping between the `Author` domain object and `AuthorCreateDto`. As soon as our application starts and initializes AutoMapper, AutoMapper will scan our application and look for classes that inherit from the Profile class and load their mapping configurations.

To use the newly created mapper in a controller, use dependency injection to have an instance of the mapper available.
```c#
private readonly IMapper _mapper;
 
public AuthorController(IMapper mapper)
{
    _mapper = mapper;
}

// Then use it somewhere
AuthorCreateDto authorCreateDto = 
    _mapper.Map<AuthorCreateDto>(Author);

```
The `Map()` method maps the `Author` object to the `AuthorCreateDto` object. No extra configuration needs to be done for this type of mapping, as the varibales have the same name and Automapper uses convention to help map. 

When there is a need for different variable names or more complex mappings, the `CreateMap()` method needs to be used in conjunction with `.ForMember()` method. To enable reverse mapping, simply append the `.ReverseMap()` method to `CreateMap()`.

AutoMapper uses a programming concept called [Reflection](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/reflection) to retrieve the type metadata of objects. We can use reflection to dynamically get the type from an existing object and invoke its methods or access its fields and properties. 

Then, based on the conventions and configurations defined, we can easily map the properties of the two types. AutoMapper was built around this concept.
### Best practices
Some good best practices include:
- Always use the AutoMapper.Extensions.Microsoft.DependencyInjection package in ASP.NET Core with `services.AddAutoMapper(assembly[])`. This package will perform all the scanning and dependency injection registration. We only need to declare the Profile configurations.
- Always organize configuration into Profiles. Profiles allow us to group common configuration and organize mappings by usage. This lets us put mapping configuration closer to where it is used, instead of a single file of configuration that becomes difficult to edit/maintain.
- Always use configuration options supported by LINQ over their counterparts as LINQ query extensions have the best performance of any mapping strategy.
- Always flatten DTOs. AutoMapper can handle mapping properties A.B.C into ABC. By flattening our model, we create a more simplified object that won’t require a lot of navigation to get at data.
- Always put common simple computed properties into the source model. Similarly, we need to place computed properties specific to the destination model in the destination model.

Typically, the following should be avoided:
- Do not call CreateMap() on each request. It is not a good practice to create the configuration for each mapping request. Mapping configuration should be done once at startup.
- Do not use inline maps. Inline maps may seem easier for very simple scenarios, but we lose the ease of configuration.
- If we have to write a complex mapping behavior, it might be better to avoid using AutoMapper for that scenario.
- Do not put any logic that is not strictly mapping behavior into the configuration. AutoMapper should not perform any business logic, it should only handle the mapping.
- Avoid sharing DTOs across multiple maps. Model your DTOs around individual actions, and if you need to change it, you only affect that action.
- Do not create DTOs with circular associations. AutoMapper does support it, but it’s confusing and can result in quite bad performance. Instead, we can create separate DTOs for each level of a hierarchy we want.



## Controllers
Controllers house the enpoints on the API and allow for seperation inside the code base. This means that there should be a controller created to handle each category of enpoint: book, author, loan, and profile. 

Each controller will inherit from the `BaseController` class which provides access to many helper methods needed for handling HTTP requests. Each endpoint also has a relevant route name, matching the required names set forth in the specificaitons. 

Creating controllers is very simple with scaffolding. This allows Visual Studio to do most of the boiler plate work to start with which is then customised to suit the needs of the applicaiton. To create a scaffolded controller:
```
Right click controllers folder > Add > New Scaffold Item > API Controller with actions, using Entity Framework
```
From here, select the model class, for this example, the `Book` class, and the context. This will generate a `BooksController` with the correct inheritance, routing, and action methods on place. 
```c#
[Route("api/[controller]")]
[ApiController]
public class BooksController : ControllerBase
{
    private readonly BookDbContext _context;

    public BooksController(BookDbContext context)
    {
        _context = context;
    }

    // GET: api/Books
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Book>>>
     GetBook()
    {
        return await _context.Book.ToListAsync();
    }
    ...
```
The same process is repeated to create a `AuthorsController` and a `LoansController`. Keep in mind that the model classes are going to be replaced by DTOs and the Automapper created is used - this was just to get the boiler plate set up to speed up development. 

The generated controllers are changed in the following ways:
- Parameters for the action methods are changed from model classes to Dtos.
- Return object types are changed to the common response object created.
- Conversion between model classes and Dtos are handled by a mapper.
- All action methods that create resources contain a server side validation step, making use of the provided methods and data annotations placed on the dtos. 
- Proper error handling with messages are caught.
- Where applicable, change endpoint names to match specs.

## Authentication
Keycloak
