﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Book
{
    public class BookReadShortDto
    {
        public int BookId { get; set; }
        public string Title { get; set; }
    }
}
