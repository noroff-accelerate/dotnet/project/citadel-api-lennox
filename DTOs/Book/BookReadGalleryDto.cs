﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using bookstore_api.DTOs;
using bookstore_api.DTOs.Author;

namespace bookstore_api.DTOs.Book
{
    public class BookReadGalleryDto
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public string CoverImg { get; set; }
        public int QtyOnHand { get; set; }
        public ICollection<AuthorDto> Authors { get; set; }
    }
}
