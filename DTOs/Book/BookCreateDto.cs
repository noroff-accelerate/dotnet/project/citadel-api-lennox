﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Book
{
    public class BookCreateDto
    {
        [MaxLength(13)]
        public string ISBN10 { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
#nullable enable
        [MaxLength(17)]
        public string? ISBN13 { get; set; }
        [MaxLength(50)]
        public string? Publisher { get; set; }
        [DataType(DataType.Date)]
        public DateTime? ReleaseDate { get; set; }
        [MaxLength(50)]
        public string? Format { get; set; }
        [MaxLength(50)]
        public string? Language { get; set; }
        public int? Length { get; set; }
        public string? Description { get; set; }
        [DataType(DataType.ImageUrl)]
        public string? CoverImg { get; set; }
#nullable disable
        public int QtyOnHand { get; set; }
        public ICollection<int> Authors { get; set; }
    }
}
