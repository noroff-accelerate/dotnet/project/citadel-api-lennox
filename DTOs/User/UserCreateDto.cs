﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.User
{
    public class UserCreateDto
    {
        [Required]
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserToken { get; set; }
        [DataType(DataType.ImageUrl)]
        public string? ProfileImg { get; set; }
    }
}
