﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.User
{
    public class UserDto
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserToken { get; set; }
        public string? ProfileImg { get; set; }
        public ICollection<int> Loans { get; set; }
    }
}
