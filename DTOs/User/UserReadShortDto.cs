﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.User
{
    public class UserReadShortDto
    {
        public string UserId {get; set;}
        public string UserName { get; set; }
    }
}
