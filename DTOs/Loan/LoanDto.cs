﻿using bookstore_api.DTOs.Book;
using bookstore_api.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Loan
{
    public class LoanDto
    {
        public int LoanId { get; set; }
        public ICollection<BookReadShortDto> Books { get; set; }
        public UserReadShortDto User { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
    }
}
