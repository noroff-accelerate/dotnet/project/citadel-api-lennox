﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Loan
{
    public class LoanCreateDto
    {    
        public ICollection<int> LoanedBooks { get; set; }    
        [Required]
        public string UserId { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}
