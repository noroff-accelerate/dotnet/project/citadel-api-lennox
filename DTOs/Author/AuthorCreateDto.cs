﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Author
{
    public class AuthorCreateDto
    {
        // Either full name or pen name (if they use pen name)
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
