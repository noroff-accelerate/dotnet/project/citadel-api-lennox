﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bookstore_api.DTOs.Author
{
    public class AuthorDto
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
    }
}
